import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SearchUserPageComponent } from './search-user-page/search-user-page.component';
import { PostsPageComponent } from './posts-page/posts-page.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'search-user',
    pathMatch: 'full'
  },
  {
    path: 'search-user',
    component: SearchUserPageComponent,
  },
  {
    path: 'user-info',
    component: PostsPageComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ComponentsRoutingModule { }
