import { Component, OnInit } from '@angular/core';
import { SessionStorage } from 'ngx-webstorage';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-posts-page',
  templateUrl: './posts-page.component.html',
  styleUrls: ['./posts-page.component.scss']
})
export class PostsPageComponent implements OnInit {
  public userName: string;
  public userPostData = [];
  @SessionStorage('userData') public userData;
  constructor(private http: HttpClient) { }

  ngOnInit() {
    this.getPostData();
  }
  getPostData(): void {
    this.http.get<any>(`https://jsonplaceholder.typicode.com/posts?userId=${this.userData[0].id}`).subscribe(data => {
      this.userPostData = data;
    });
  }
}
