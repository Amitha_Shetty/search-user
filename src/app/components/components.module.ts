import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ComponentsRoutingModule } from './components-routing.module';
import { SearchUserPageComponent } from './search-user-page/search-user-page.component';
import { PostsPageComponent } from './posts-page/posts-page.component';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [SearchUserPageComponent, PostsPageComponent],
  imports: [
    CommonModule,
    ComponentsRoutingModule,
    FormsModule,
  ]
})
export class ComponentsModule { }
