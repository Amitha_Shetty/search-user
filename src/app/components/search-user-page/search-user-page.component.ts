import { Component, OnInit } from '@angular/core';
import swal from 'sweetalert2';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { SessionStorage, SessionStorageService } from 'ngx-webstorage';


@Component({
  selector: 'app-search-user-page',
  templateUrl: './search-user-page.component.html',
  styleUrls: ['./search-user-page.component.scss']
})
export class SearchUserPageComponent implements OnInit {
  public isSearchText = false;
  public searchData: string;
  @SessionStorage('userData') public userData;
  constructor(private http: HttpClient , private router: Router , private sessionStorage: SessionStorageService) { }

  ngOnInit( ) {
    this.sessionStorage.clear();
  }
  searchUser(event): void {
    const validStr = /^[a-zA-Z0-9][a-zA-Z0-9\s]*$/;
    if (event.target.value && event.target.value.match(validStr)) {
      this.isSearchText = true;
    } else {
      this.isSearchText = false;
      if (event.key === 'Enter') {
        swal.fire('Plese Enter The Valid Text');
      }
    }
  }
  onSubmit(): void {
    this.http.get<any>(`https://jsonplaceholder.typicode.com/users?name=${this.searchData}`).subscribe(data => {
      if (data.length > 0) {
        this.userData = data;
        this.router.navigate(['user-info']);
      } else {
        swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: `We Couldn't find the user!`
        });
      }
  });
  }

}
